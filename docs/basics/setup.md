Setup and Main Class
====================
###Setting up the Modding Workspace
#####Note: This tutorial assumes you are using Intellij as your IDE. We highly recommend using Intellij for StarMade modding, as all guides and tutorials in this documentation are written for it.
1. Ensure that StarLoader is installed onto your StarMade installation. In future, StarLoader will be included
as part of StarMade release builds. If you are unsure how to do this, please see the Installation page.
2. Navigate to your StarMade installation folder. It should contain a file called StarMade.jar. Add the StarMade.jar file
as a dependency for your project.
3. While still in your StarMade installation folder, find the lib folder and also add it as a dependency for your project.
4. (Optional) If you are using StarAPI, place it in your mods folder in your StarMade installation directory and add it
as a dependency to your project.
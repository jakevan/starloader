//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package org.schema.game.client.view.effects;

import org.schema.game.client.data.GameClientState;
import org.schema.game.common.data.world.RemoteSector;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.core.Drawable;
import org.schema.schine.graphicsengine.forms.Sprite;
import org.schema.schine.resource.ResourceLoader;

import java.util.ArrayList;

@Deprecated
public class ItemDrawer implements Drawable {
    private GameClientState state;
    private boolean init;
    private Sprite[] sprite;

    public ItemDrawer(GameClientState var1) {
        this.state = var1;
    }

    public void cleanUp() {
    }

    public void draw() {
        if (!this.init) {
            this.onInit();
        }

        RemoteSector var1;
        if ((var1 = this.state.getCurrentRemoteSector()) != null && !var1.getItems().isEmpty()) {
            for(int var2 = 0; var2 < this.sprite.length; ++var2) {
                Sprite var3;
                (var3 = this.sprite[var2]).setScale(0.01F, 0.01F, 0.01F);
                var3.setFlip(true);
                var3.setBillboard(true);
                Sprite.draw3D(var3, var1.getItems().values(), Controller.getCamera());
                var3.setBillboard(false);
                var3.setFlip(false);
                var3.setScale(1.0F, 1.0F, 1.0F);
            }

        }
    }

    public boolean isInvisible() {
        return false;
    }
    //INSERTED CODE
    public void onInit() {
        ResourceLoader resLoader = Controller.getResLoader();
        ArrayList<Sprite> sprites = new ArrayList<>();
        for (String s : resLoader.getImageLoader().getSpriteMap().keySet()) {
            if(s.startsWith("build-icons-") || s.startsWith("meta-icons-")){
                sprites.add(resLoader.getSprite(s));
            }
        }
        this.sprite = sprites.toArray(new Sprite[0]);
        this.init = true;
    }
    ///
}

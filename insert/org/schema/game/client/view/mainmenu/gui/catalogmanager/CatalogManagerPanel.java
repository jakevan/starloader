package org.schema.game.client.view.mainmenu.gui.catalogmanager;

import api.smd.SMDEntryUtils;
import api.utils.gui.SimpleGUIHorizontalButtonPane;
import api.utils.gui.SimpleGUIVerticalButtonPane;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.apache.commons.io.FileUtils;
import org.schema.game.client.controller.PlayerOkCancelInput;
import org.schema.game.client.controller.PlayerTextInput;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.client.view.mainmenu.FileChooserDialog;
import org.schema.game.client.view.mainmenu.MainMenuGUI;
import org.schema.game.client.view.mainmenu.gui.FileChooserStats;
import org.schema.game.server.controller.BluePrintController;
import org.schema.game.server.controller.CatalogEntryNotFoundException;
import org.schema.game.server.controller.ImportFailedException;
import org.schema.game.server.data.blueprintnw.BlueprintEntry;
import org.schema.schine.common.TextCallback;
import org.schema.schine.common.language.Lng;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.settings.PrefixNotFoundException;
import org.schema.schine.graphicsengine.forms.gui.*;
import org.schema.schine.graphicsengine.forms.gui.newgui.DialogInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIActiveInterface;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIContentPane;
import org.schema.schine.graphicsengine.forms.gui.newgui.GUIMainWindow;
import org.schema.schine.input.InputState;
import javax.swing.filechooser.FileFilter;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CatalogManagerPanel extends GUIElement implements GUIActiveInterface {

    private static BlueprintEntryScrollableTableList blueprintList;
    private static TemplateScrollableList templateList;
    private boolean init;
    public GUIMainWindow mainPanel;
    private GUIContentPane blueprintsTab;
    private GUIContentPane templatesTab;
    private GUIContentPane browseTab;
    private DialogInput dialogInput;
    private List<GUIElement> toCleanUp = new ObjectArrayList<>();

    public CatalogManagerPanel(InputState state, DialogInput dialogInput) {
        super(state);
        this.dialogInput = dialogInput;
    }

    @Override
    public float getWidth() {
        return GLFrame.getWidth() - 470;
    }

    @Override
    public float getHeight() {
        return GLFrame.getHeight() - 70;
    }

    @Override
    public void cleanUp() {
        for(GUIElement e : toCleanUp) {
            e.cleanUp();
        }
        toCleanUp.clear();
    }

    @Override
    public void draw() {
        if(!init) {
            onInit();
        }
        GlUtil.glPushMatrix();
        transform();

        mainPanel.draw();

        GlUtil.glPopMatrix();
    }

    @Override
    public void onInit() {
        if(init) {
            return;
        }
        mainPanel = new GUIMainWindow(getState(), GLFrame.getWidth() - 410, GLFrame.getHeight() - 20, 400, 10, "Catalog Manager");
        mainPanel.onInit();
        mainPanel.setPos(435, 35, 0);
        mainPanel.setWidth(GLFrame.getWidth() - 470);
        mainPanel.setHeight(GLFrame.getHeight() - 70);
        mainPanel.clearTabs();

        createBlueprintListTab();
        blueprintsTab.onInit();
        createTemplatesTab();
        templatesTab.onInit();
        createBrowseTab();
        browseTab.onInit();

        mainPanel.setSelectedTab(0);
        mainPanel.activeInterface = this;

        mainPanel.setCloseCallback(new GUICallback() {

            @Override
            public boolean isOccluded() {
                return !isActive();
            }

            @Override
            public void callback(GUIElement callingGuiElement, MouseEvent event) {
                if(event.pressedLeftMouse()){
                    dialogInput.deactivate();
                }
            }
        });

        toCleanUp.add(mainPanel);
        init = true;
    }

    @Override
    public boolean isInside() {
        return mainPanel.isInside();
    }

    private void createBlueprintListTab() {
        blueprintsTab = mainPanel.addTab("BLUEPRINTS");
        blueprintsTab.setTextBoxHeightLast(800);
        blueprintList = new BlueprintEntryScrollableTableList(blueprintsTab.getState(), blueprintsTab.getContent(0));
        blueprintList.onInit();
        blueprintsTab.getContent(0).attach(blueprintList);
        toCleanUp.add(blueprintList);

        blueprintsTab.addNewTextBox(32);
        SimpleGUIHorizontalButtonPane buttonPane = new SimpleGUIHorizontalButtonPane(getState(), blueprintsTab.getWidth() - 76, 30, 2);
        GUITextButton importButton = new GUITextButton(getState(), 150, 24, GUITextButton.ColorPalette.OK, "IMPORT", new GUICallback() {
            @Override
            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                if(mouseEvent.pressedLeftMouse()) {
                    (new FileChooserDialog(getState(), new FileChooserStats(getState(), new File("./"), "", new FileFilter() {
                        @Override
                        public String getDescription() {
                            return "StarMade Blueprint (.sment)";
                        }

                        @Override
                        public boolean accept(File file) {
                            return file.isDirectory() || file.getName().toLowerCase(Locale.ENGLISH).endsWith(".sment");
                        }
                    }) {

                        @Override
                        public void onSelectedFile(File dir, String file) {
                            final File f = new File(dir, file);
                            ArrayList<String> blueprintNames = new ArrayList<>();
                            for(BlueprintEntry BP : BluePrintController.active.readBluePrints()) {
                                blueprintNames.add(BP.getName().substring(0, BP.getName().indexOf(".sment")));
                            }
                            String bpName = f.getName().substring(0, f.getName().indexOf(".sment"));
                            if(f.exists() && !(blueprintNames.contains(bpName))) {
                                try {
                                    System.err.println("IMPORTING BLUEPRINT " + f.getCanonicalPath());
                                    BluePrintController.active.importFile(f, null);
                                    blueprintList.updated = false;
                                    blueprintList.updateBlueprints();
                                } catch (IOException | ImportFailedException e) {
                                    e.printStackTrace();
                                }
                            } else if(f.exists() && blueprintNames.contains(bpName)) {
                                PlayerOkCancelInput playerInput = new PlayerOkCancelInput("CONFIRM", getState(), 300, 150, Lng.str("Error"), "A Blueprint with the name " + bpName + " already exists in your catalog!") {
                                    @Override
                                    public void pressedOK() {
                                        deactivate();
                                    }
                                    @Override
                                    public void onDeactivate() { }
                                };
                                playerInput.getInputPanel().onInit();
                                playerInput.getInputPanel().setCancelButton(false);
                                playerInput.getInputPanel().background.setPos(470.0F, 35.0F, 0.0F);
                                playerInput.getInputPanel().background.setWidth((float)(GLFrame.getWidth() - 435));
                                playerInput.getInputPanel().background.setHeight((float)(GLFrame.getHeight() - 70));
                                playerInput.activate();
                            } else {
                                PlayerOkCancelInput playerInput = new PlayerOkCancelInput("CONFIRM", getState(), 300, 150, Lng.str("Error"), Lng.str("The file %s doesn't exist", bpName)) {
                                    @Override
                                    public void pressedOK() {
                                        deactivate();
                                    }
                                    @Override
                                    public void onDeactivate() { }
                                };
                                playerInput.getInputPanel().onInit();
                                playerInput.getInputPanel().setCancelButton(false);
                                playerInput.getInputPanel().background.setPos(470.0F, 35.0F, 0.0F);
                                playerInput.getInputPanel().background.setWidth((float)(GLFrame.getWidth() - 435));
                                playerInput.getInputPanel().background.setHeight((float)(GLFrame.getHeight() - 70));
                                playerInput.activate();
                            }
                        }
                    })).activate();
                }
            }

            @Override
            public boolean isOccluded() {
                return !isActive();
            }
        });
        buttonPane.addButton(importButton);

        GUITextButton exportButton = new GUITextButton(getState(), 150, 24, GUITextButton.ColorPalette.OK, "EXPORT", new GUICallback() {
            @Override
            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                if(mouseEvent.pressedLeftMouse()) {
                    if(blueprintList.getSelectedRow() != null && blueprintList.getSelectedRow().f != null) {
                        getState().getController().queueUIAudio("0022_menu_ui - enter");
                        try {
                            BluePrintController.active.export(blueprintList.getSelectedRow().f.getName());
                        } catch (IOException | CatalogEntryNotFoundException e) {
                            e.printStackTrace();
                        }
                        blueprintList.updated = false;
                        blueprintList.updateBlueprints();
                    }
                }
            }

            @Override
            public boolean isOccluded() {
                return !isActive();
            }
        });
        buttonPane.addButton(exportButton);

        GUITextButton deleteButton = new GUITextButton(getState(), 150, 24, GUITextButton.ColorPalette.CANCEL, "DELETE", new GUICallback() {
            @Override
            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                if(mouseEvent.pressedLeftMouse()) {
                    if(blueprintList.getSelectedRow() != null && blueprintList.getSelectedRow().f != null) {
                        getState().getController().queueUIAudio("0022_menu_ui - enter");
                        PlayerOkCancelInput confirmBox = new PlayerOkCancelInput("CONFIRM", getState(), 300, 150,"Confirm Deletion", "This will remove the blueprint from your catalog and delete it on disk. Proceed?") {
                            @Override
                            public void pressedOK() {
                                try {
                                    BluePrintController.active.removeBluePrint(blueprintList.getSelectedRow().f);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                blueprintList.updated = false;
                                blueprintList.updateBlueprints();
                                deactivate();
                            }

                            @Override
                            public void onDeactivate() {
                            }
                        };
                        confirmBox.getInputPanel().onInit();
                        confirmBox.getInputPanel().background.setPos(470.0F, 35.0F, 0.0F);
                        confirmBox.getInputPanel().background.setWidth((float)(GLFrame.getWidth() - 435));
                        confirmBox.getInputPanel().background.setHeight((float)(GLFrame.getHeight() - 70));
                        confirmBox.activate();
                    }
                }

            }

            @Override
            public boolean isOccluded() {
                return !isActive();
            }
        });
        buttonPane.addButton(deleteButton);

        blueprintsTab.getContent(1).attach(buttonPane);
        toCleanUp.add(importButton);
        toCleanUp.add(exportButton);
        toCleanUp.add(deleteButton);
        toCleanUp.add(buttonPane);
        toCleanUp.add(blueprintsTab);
    }

    private void createTemplatesTab() {
        templatesTab = mainPanel.addTab("TEMPLATES");
        templatesTab.setTextBoxHeightLast(800);

        templateList = new TemplateScrollableList(templatesTab.getState(), templatesTab.getContent(0));
        templateList.onInit();
        templatesTab.getContent(0).attach(templateList);
        toCleanUp.add(templateList);

        templatesTab.addNewTextBox(32);
        SimpleGUIHorizontalButtonPane buttonPane = new SimpleGUIHorizontalButtonPane(getState(), templatesTab.getWidth() - 76, 30, 2);

        GUITextButton importButton = new GUITextButton(getState(), 150, 24, GUITextButton.ColorPalette.OK, "IMPORT", new GUICallback() {
            @Override
            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                if(mouseEvent.pressedLeftMouse()) {
                    if(templateList.getSelectedRow() != null && templateList.getSelectedRow().f != null) {
                        getState().getController().queueUIAudio("0022_menu_ui - enter");
                        (new FileChooserDialog(getState(), new FileChooserStats(getState(), new File("./templates"), "", new FileFilter() {
                            @Override
                            public String getDescription() {
                                return "StarMade Template (.smtpl)";
                            }

                            @Override
                            public boolean accept(File file) {
                                return file.isDirectory() || file.getName().toLowerCase(Locale.ENGLISH).endsWith(".smtpl");
                            }
                        }) {

                            @Override
                            public void onSelectedFile(File dir, String file) {
                                final File f = new File(dir, file);
                                ArrayList<String> templateNames = new ArrayList<>();
                                File templatesFolder = new File("./templates");
                                if(!templatesFolder.exists()) templatesFolder.mkdirs();
                                if(templatesFolder.listFiles() != null) {
                                    for(File templateFile : templatesFolder.listFiles()) {
                                        templateNames.add(templateFile.getName().substring(0, templateFile.getName().indexOf(".smtpl") - 1));
                                    }
                                }
                                String templateName = f.getName().substring(0, f.getName().indexOf(".smtpl"));
                                if(f.exists() && !(templateNames.contains(templateName))) {
                                    try {
                                        System.err.println("IMPORTING TEMPLATE " + f.getCanonicalPath());
                                        FileUtils.copyFile(f, new File(templatesFolder.getAbsolutePath() + "/" + f.getName()));
                                        templateList.updated = false;
                                        templateList.updateTemplates();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                } else if(f.exists() && templateNames.contains(templateName)) {
                                    PlayerOkCancelInput playerInput = new PlayerOkCancelInput("CONFIRM", getState(), 300, 150, Lng.str("Error"), "A Template with the name " + templateName + " already exists in your catalog!") {
                                        @Override
                                        public void pressedOK() {
                                            deactivate();
                                        }
                                        @Override
                                        public void onDeactivate() { }
                                    };
                                    playerInput.getInputPanel().onInit();
                                    playerInput.getInputPanel().setCancelButton(false);
                                    playerInput.getInputPanel().background.setPos(470.0F, 35.0F, 0.0F);
                                    playerInput.getInputPanel().background.setWidth((float)(GLFrame.getWidth() - 435));
                                    playerInput.getInputPanel().background.setHeight((float)(GLFrame.getHeight() - 70));
                                    playerInput.activate();
                                } else {
                                    PlayerOkCancelInput playerInput = new PlayerOkCancelInput("CONFIRM", getState(), 300, 150, Lng.str("Error"), Lng.str("The file %s doesn't exist", templateName)) {
                                        @Override
                                        public void pressedOK() {
                                            deactivate();
                                        }
                                        @Override
                                        public void onDeactivate() { }
                                    };
                                    playerInput.getInputPanel().onInit();
                                    playerInput.getInputPanel().setCancelButton(false);
                                    playerInput.getInputPanel().background.setPos(470.0F, 35.0F, 0.0F);
                                    playerInput.getInputPanel().background.setWidth((float)(GLFrame.getWidth() - 435));
                                    playerInput.getInputPanel().background.setHeight((float)(GLFrame.getHeight() - 70));
                                    playerInput.activate();
                                }
                            }
                        })).activate();

                        templateList.updated = false;
                        templateList.updateTemplates();
                    }
                }
            }

            @Override
            public boolean isOccluded() {
                return !isActive();
            }
        });
        buttonPane.addButton(importButton);

        GUITextButton renameButton = new GUITextButton(getState(), 150, 24, GUITextButton.ColorPalette.OK, "RENAME", new GUICallback() {
            @Override
            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                if(mouseEvent.pressedLeftMouse()) {
                    if (templateList.getSelectedRow() != null && templateList.getSelectedRow().f != null) {
                        getState().getController().queueUIAudio("0022_menu_ui - enter");
                        final ArrayList<String> fileNames = new ArrayList<>();
                        final File templatesFolder = new File("./templates");
                        if (!templatesFolder.exists()) templatesFolder.mkdirs();
                        if (templatesFolder.listFiles() != null) {
                            for (File templateFile : templatesFolder.listFiles()) {
                                fileNames.add(templateFile.getName().substring(0, templateFile.getName().indexOf(".smtpl")));
                            }
                        }

                        PlayerTextInput textInputBox = new PlayerTextInput("RENAME_TEMPLATE", getState(), 64, "Enter a new name", "Enter a new name for template " + templateList.getSelectedRow().f.getName().substring(0, templateList.getSelectedRow().f.getName().indexOf(".smtpl"))) {
                            @Override
                            public void onDeactivate() {

                            }

                            @Override
                            public boolean onInput(String s) {
                                if (fileNames.contains(s)) {
                                    PlayerOkCancelInput playerInput = new PlayerOkCancelInput("CONFIRM", getState(), 300, 150, Lng.str("Error"), "A Template with the name " + s + " already exists in your catalog!") {
                                        @Override
                                        public void pressedOK() {
                                            deactivate();
                                        }

                                        @Override
                                        public void onDeactivate() {
                                        }
                                    };
                                    playerInput.getInputPanel().onInit();
                                    playerInput.getInputPanel().setCancelButton(false);
                                    playerInput.getInputPanel().background.setPos(470.0F, 35.0F, 0.0F);
                                    playerInput.getInputPanel().background.setWidth((float) (GLFrame.getWidth() - 435));
                                    playerInput.getInputPanel().background.setHeight((float) (GLFrame.getHeight() - 70));
                                    playerInput.activate();
                                    return false;
                                } else {
                                    try {
                                        File newTemplate = new File(templatesFolder.getAbsolutePath() + "/" + s + ".smtpl");
                                        newTemplate.createNewFile();
                                        FileUtils.copyFile(templateList.getSelectedRow().f, newTemplate);
                                        FileUtils.forceDelete(templateList.getSelectedRow().f);
                                        templateList.updated = false;
                                        templateList.updateTemplates();
                                        return true;
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                        return false;
                                    }
                                }
                            }

                            @Override
                            public String[] getCommandPrefixes() {
                                return null;
                            }

                            @Override
                            public String handleAutoComplete(String s, TextCallback textCallback, String s1) throws PrefixNotFoundException {
                                return null;
                            }

                            @Override
                            public void onFailedTextCheck(String s) {

                            }
                        };
                        textInputBox.getInputPanel().onInit();
                        textInputBox.getInputPanel().background.setPos(470.0F, 35.0F, 0.0F);
                        textInputBox.getInputPanel().background.setWidth((float) (GLFrame.getWidth() - 435));
                        textInputBox.getInputPanel().background.setHeight((float) (GLFrame.getHeight() - 70));
                        textInputBox.activate();
                    }

                    templateList.updated = false;
                    templateList.updateTemplates();
                }
            }

            @Override
            public boolean isOccluded() {
                return !isActive();
            }
        });
        buttonPane.addButton(renameButton);

        GUITextButton deleteButton = new GUITextButton(getState(), 150, 24, GUITextButton.ColorPalette.CANCEL, "DELETE", new GUICallback() {
            @Override
            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                if(mouseEvent.pressedLeftMouse()) {
                    if(templateList.getSelectedRow() != null && templateList.getSelectedRow().f != null) {
                        getState().getController().queueUIAudio("0022_menu_ui - enter");
                        PlayerOkCancelInput confirmBox = new PlayerOkCancelInput("CONFIRM", getState(), 300, 150,"Confirm Deletion", "This will remove the template from your catalog and delete it on disk. Proceed?") {
                            @Override
                            public void pressedOK() {
                                try {
                                    FileUtils.forceDelete(templateList.getSelectedRow().f);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                templateList.updated = false;
                                templateList.updateTemplates();
                                deactivate();
                            }

                            @Override
                            public void onDeactivate() {
                            }
                        };
                        confirmBox.getInputPanel().onInit();
                        confirmBox.getInputPanel().background.setPos(470.0F, 35.0F, 0.0F);
                        confirmBox.getInputPanel().background.setWidth((float)(GLFrame.getWidth() - 435));
                        confirmBox.getInputPanel().background.setHeight((float)(GLFrame.getHeight() - 70));
                        confirmBox.activate();
                    }
                }
            }

            @Override
            public boolean isOccluded() {
                return !isActive();
            }
        });
        buttonPane.addButton(deleteButton);

        templatesTab.getContent(1).attach(buttonPane);
        toCleanUp.add(importButton);
        toCleanUp.add(renameButton);
        toCleanUp.add(deleteButton);
        toCleanUp.add(buttonPane);
        toCleanUp.add(templatesTab);
    }

    private void createBrowseTab() {
        SMDEntryUtils.fetchDataOnThread();
        browseTab = mainPanel.addTab("BROWSE");
        browseTab.addDivider(116);

        final SMDContentScrollableList contentList = new SMDContentScrollableList(getState(), browseTab.getContent(1, 0));
        contentList.onInit();

        SimpleGUIVerticalButtonPane buttonPane = new SimpleGUIVerticalButtonPane(getState(), 24, browseTab.getHeight() - 94, 2);
        GUITextButton allButton = new GUITextButton(getState(), 100, 30, GUITextButton.ColorPalette.OK, "ALL", new GUICallback() {
            @Override
            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                if(mouseEvent.pressedLeftMouse()) {
                    contentList.setCurrentFilter(FilterType.ALL);
                }
            }

            @Override
            public boolean isOccluded() {
                return false;
            }
        });
        buttonPane.addButton(allButton);

        GUITextButton allShipsButton = new GUITextButton(getState(), 100, 30, GUITextButton.ColorPalette.OK, "SHIPS", new GUICallback() {
            @Override
            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                if(mouseEvent.pressedLeftMouse()) {
                    contentList.setCurrentFilter(FilterType.SHIPS_ALL);
                }
            }

            @Override
            public boolean isOccluded() {
                return false;
            }
        });
        buttonPane.addButton(allShipsButton);

        GUITextButton rpShipsButton = new GUITextButton(getState(), 100, 30, GUITextButton.ColorPalette.OK, "RP SHIPS", new GUICallback() {
            @Override
            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                if(mouseEvent.pressedLeftMouse()) {
                    contentList.setCurrentFilter(FilterType.SHIPS_RP);
                }
            }

            @Override
            public boolean isOccluded() {
                return false;
            }
        });
        buttonPane.addButton(rpShipsButton);

        GUITextButton pvpShipsButton = new GUITextButton(getState(), 100, 30, GUITextButton.ColorPalette.OK, "PVP SHIPS", new GUICallback() {
            @Override
            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                if(mouseEvent.pressedLeftMouse()) {
                    contentList.setCurrentFilter(FilterType.SHIPS_PVP);
                }
            }

            @Override
            public boolean isOccluded() {
                return false;
            }
        });
        buttonPane.addButton(pvpShipsButton);

        GUITextButton shipShellsButton = new GUITextButton(getState(), 100, 30, GUITextButton.ColorPalette.OK, "SHIP SHELLS", new GUICallback() {
            @Override
            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                if(mouseEvent.pressedLeftMouse()) {
                    contentList.setCurrentFilter(FilterType.SHIPS_SHELLS);
                }
            }

            @Override
            public boolean isOccluded() {
                return false;
            }
        });
        buttonPane.addButton(shipShellsButton);

        GUITextButton allStationsButton = new GUITextButton(getState(), 100, 30, GUITextButton.ColorPalette.OK, "STATIONS", new GUICallback() {
            @Override
            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                if(mouseEvent.pressedLeftMouse()) {
                    contentList.setCurrentFilter(FilterType.STATIONS_ALL);
                }
            }

            @Override
            public boolean isOccluded() {
                return false;
            }
        });
        buttonPane.addButton(allStationsButton);

        GUITextButton rpStationsButton = new GUITextButton(getState(), 100, 30, GUITextButton.ColorPalette.OK, "RP STATIONS", new GUICallback() {
            @Override
            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                if(mouseEvent.pressedLeftMouse()) {
                    contentList.setCurrentFilter(FilterType.STATIONS_RP);
                }
            }

            @Override
            public boolean isOccluded() {
                return false;
            }
        });
        buttonPane.addButton(rpStationsButton);

        GUITextButton pvpStationsButton = new GUITextButton(getState(), 100, 30, GUITextButton.ColorPalette.OK, "PVP STATIONS", new GUICallback() {
            @Override
            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                if(mouseEvent.pressedLeftMouse()) {
                    contentList.setCurrentFilter(FilterType.STATIONS_PVP);
                }
            }

            @Override
            public boolean isOccluded() {
                return false;
            }
        });
        buttonPane.addButton(pvpStationsButton);

        GUITextButton stationsShellsButton = new GUITextButton(getState(), 100, 30, GUITextButton.ColorPalette.OK, "STATION SHELLS", new GUICallback() {
            @Override
            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                if(mouseEvent.pressedLeftMouse()) {
                    contentList.setCurrentFilter(FilterType.STATIONS_SHELLS);
                }
            }

            @Override
            public boolean isOccluded() {
                return false;
            }
        });
        buttonPane.addButton(stationsShellsButton);

        GUITextButton logicButton = new GUITextButton(getState(), 100, 30, GUITextButton.ColorPalette.OK, "LOGIC", new GUICallback() {
            @Override
            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                if(mouseEvent.pressedLeftMouse()) {
                    contentList.setCurrentFilter(FilterType.LOGIC);
                }
            }

            @Override
            public boolean isOccluded() {
                return false;
            }
        });
        buttonPane.addButton(logicButton);

        GUITextButton turretsButton = new GUITextButton(getState(), 100, 30, GUITextButton.ColorPalette.OK, "TURRETS", new GUICallback() {
            @Override
            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                if(mouseEvent.pressedLeftMouse()) {
                    contentList.setCurrentFilter(FilterType.TURRETS);
                }
            }

            @Override
            public boolean isOccluded() {
                return false;
            }
        });
        buttonPane.addButton(turretsButton);

        GUITextButton templatesButton = new GUITextButton(getState(), 100, 30, GUITextButton.ColorPalette.OK, "TEMPLATES", new GUICallback() {
            @Override
            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                if(mouseEvent.pressedLeftMouse()) {
                    contentList.setCurrentFilter(FilterType.TEMPLATES);
                }
            }

            @Override
            public boolean isOccluded() {
                return false;
            }
        });
        buttonPane.addButton(templatesButton);

        buttonPane.setPos(0, 8, 0);
        browseTab.getContent(0, 0).attach(buttonPane);
        browseTab.getContent(1, 0).attach(contentList);
        toCleanUp.add(contentList);
        toCleanUp.add(buttonPane);
        toCleanUp.add(browseTab);
    }

    @Override
    public boolean isActive() {
        List<DialogInterface> playerInputs = getState().getController().getInputController().getPlayerInputs();
        return !MainMenuGUI.runningSwingDialog && (playerInputs.isEmpty() || playerInputs.get(playerInputs.size()-1).getInputPanel() == this);
    }

    public static void updateLists() {
        if(blueprintList != null) blueprintList.updateBlueprints();
        if(templateList != null) templateList.updateTemplates();
    }
}

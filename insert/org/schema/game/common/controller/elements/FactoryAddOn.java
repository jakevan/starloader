//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package org.schema.game.common.controller.elements;

import api.listener.fastevents.FactoryManufactureListener;
import api.listener.fastevents.FastListenerCommon;
import it.unimi.dsi.fastutil.ints.IntCollection;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.objects.Object2ObjectOpenHashMap;
import org.schema.game.common.controller.SegmentController;
import org.schema.game.common.controller.elements.factory.FactoryCollectionManager;
import org.schema.game.common.controller.elements.factory.FactoryElementManager;
import org.schema.game.common.controller.elements.factory.FactoryProducerInterface;
import org.schema.game.common.controller.elements.factory.FactoryUnit;
import org.schema.game.common.data.element.ElementInformation;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.element.FactoryResource;
import org.schema.game.common.data.element.meta.RecipeInterface;
import org.schema.game.common.data.player.inventory.Inventory;
import org.schema.game.server.data.GameServerState;
import org.schema.schine.graphicsengine.core.Timer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

@Deprecated
public class FactoryAddOn {
    public final HashMap<Short, ManagerModuleCollection<FactoryUnit, FactoryCollectionManager, FactoryElementManager>> map = new HashMap();
    private final Object2ObjectOpenHashMap<Inventory, IntOpenHashSet> changedSet = new Object2ObjectOpenHashMap();
    private SegmentController segmentController;
    private boolean initialized;

    public FactoryAddOn() {
    }

    public static FactoryResource[] getInputType(RecipeInterface var0, int var1) {
        assert var0.getRecipeProduct() != null : var0;

        assert var0.getRecipeProduct()[var1] != null : var0.getRecipeProduct();

        return var0.getRecipeProduct()[var1].getInputResource();
    }

    public static FactoryResource[] getOutputType(RecipeInterface var0, int var1) {
        return var0.getRecipeProduct()[var1].getOutputResource();
    }

    public static int getProductCount(RecipeInterface var0) {
        return var0.getRecipeProduct().length;
    }

    public static int getCount(FactoryResource var0) {
        return var0.count;
    }

    public static void produce(RecipeInterface recipe, int var1, Inventory inv, FactoryProducerInterface var3, IntCollection var4, GameServerState var5) {
        int var6 = inv.getProductionLimit() > 0 ? inv.getProductionLimit() : 2147483647;
        boolean var7 = true;
        int var8 = var3.getFactoryCapability();
        FactoryResource[] var9;
        int var10 = (var9 = getInputType(recipe, var1)).length;

        int var11;
        for(var11 = 0; var11 < var10; ++var11) {
            FactoryResource var12 = var9[var11];
            int var13;
            if ((var13 = inv.getOverallQuantity(var12.type)) < getCount(var12) * var8) {
                if (var13 == 0 || var13 < getCount(var12)) {
                    var7 = false;
                    break;
                }

                var8 = var13 / getCount(var12);

                assert var13 >= getCount(var12) * var8;
            }
        }

        if (var7) {
            int var15 = 0;
            FactoryResource[] var16;
            int var17;
            FactoryResource var18;
            if (inv.getProductionLimit() > 0) {
                var11 = (var16 = getOutputType(recipe, var1)).length;

                for(var17 = 0; var17 < var11; ++var17) {
                    var18 = var16[var17];
                    var15 = Math.max(var15, inv.getOverallQuantity(var18.type));
                }

                var10 = var8;
                if (var8 > var6) {
                    var10 = var6;
                }

                if (var15 + var10 > var6) {
                    var10 = Math.max(0, var6 - var15);
                }

                var8 = var10;
            }

            if (var8 > 0) {
                var11 = (var16 = getInputType(recipe, var1)).length;

                int var14;
                for(var17 = 0; var17 < var11; ++var17) {
                    var18 = var16[var17];
                    var6 = inv.getOverallQuantity(var18.type);
                    inv.deleteAllSlotsWithType(var18.type, var4);
                    var6 -= getCount(var18) * var8;
                    var14 = inv.incExistingOrNextFreeSlot(var18.type, var6);
                    var4.add(var14);
                    //INSERTED CODE
                    for (FactoryManufactureListener listener : FastListenerCommon.factoryManufactureListeners) {
                        listener.onProduceItem(recipe, inv, var3, var18, var6, var4);
                    }
                    ///
                }

                var11 = (var16 = getOutputType(recipe, var1)).length;

                for(var17 = 0; var17 < var11; ++var17) {
                    var6 = getCount(var18 = var16[var17]) * var8;
                    var14 = inv.incExistingOrNextFreeSlot(var18.type, var6);
                    var3.getCurrentRecipe().producedGood(var6, var5);
                    var4.add(var14);
                    //INSERTED CODE
                    for (FactoryManufactureListener listener : FastListenerCommon.factoryManufactureListeners) {
                        listener.onProduceItem(recipe, inv, var3, var18, var6, var4);
                    }
                    ///
                }
            }
        }

    }

    public void initialize(List<ManagerModule<?, ?, ?>> var1, SegmentController var2) {
        Iterator var3 = ElementKeyMap.getFactorykeyset().iterator();

        while(var3.hasNext()) {
            ElementInformation var4 = ElementKeyMap.getInfo((Short)var3.next());

            assert var4.getFactory() != null;

            ManagerModuleCollection var5 = new ManagerModuleCollection(new FactoryElementManager(var2, var4.getId(), var4.getFactory().enhancer), var4.getId(), var4.getFactory().enhancer);
            var1.add(var5);
            this.map.put(var4.getId(), var5);
        }

        this.segmentController = var2;
        this.initialized = true;
    }

    public void update(Timer var1, boolean var2) {
        assert this.initialized;

        int var11 = 0;
        int var12 = 0;
        long var5 = System.currentTimeMillis();
        Iterator var7 = this.map.values().iterator();

        while(var7.hasNext()) {
            ManagerModuleCollection var8;
            Iterator var3 = (var8 = (ManagerModuleCollection)var7.next()).getCollectionManagers().iterator();

            while(var3.hasNext()) {
                FactoryCollectionManager var4 = (FactoryCollectionManager)var3.next();
                long var9;
                if ((var9 = this.segmentController.getState().getController().getServerRunningTime() / var4.getBakeTime()) > var4.lastStep) {
                    var4.manufractureStep((FactoryElementManager)var8.getElementManager(), this.changedSet);
                    ++var11;
                    var4.lastStep = var9;
                }
            }
        }

        var7 = this.changedSet.entrySet().iterator();

        while(var7.hasNext()) {
            Entry var15;
            if (!((IntOpenHashSet)(var15 = (Entry)var7.next()).getValue()).isEmpty()) {
                IntOpenHashSet var13;
                (var13 = new IntOpenHashSet()).addAll((IntCollection)var15.getValue());
                ((Inventory)var15.getKey()).sendInventoryModification(var13);
                ((IntOpenHashSet)var15.getValue()).clear();
                ++var12;
                break;
            }
        }

        long var14;
        if ((var14 = System.currentTimeMillis() - var5) > 100L) {
            System.err.println("[FACTORY] CALCULATION TOOK " + var14 + "MS with " + var11 + " steps, and " + var12 + " inventories sent");
        }

    }
}

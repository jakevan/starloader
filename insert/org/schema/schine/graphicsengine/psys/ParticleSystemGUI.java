package org.schema.schine.graphicsengine.psys;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;
import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix3f;
import javax.xml.transform.Transformer;

import com.bulletphysics.linearmath.Transform;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.network.StateInterface;
import org.schema.schine.network.client.ClientState;
import org.schema.schine.resource.FileExt;

public class ParticleSystemGUI extends JFrame {
    private static final long serialVersionUID = 1L;
    private ParticleSystemConfiguration currentSystem;
    private JFileChooser fc;
    private JPanel panel_1;

    public ParticleSystemGUI(final StateInterface var1) {
        this.setTitle("StarMade ParticleSystem");
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setBounds(GLFrame.getWidth() - 40, 50, 690, 578);
        JMenuBar var2 = new JMenuBar();
        this.setJMenuBar(var2);
        JMenu var3 = new JMenu("File");
        var2.add(var3);
        JMenuItem var8;
        (var8 = new JMenuItem("New")).addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                ParticleSystemGUI.this.currentSystem = ParticleSystemConfiguration.fromScratch();
                ParticleSystemGUI.this.updateCurrentSystem();
            }
        });
        var3.add(var8);
        (var8 = new JMenuItem("Open")).addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                ParticleSystemGUI.this.openOpenFile();
            }
        });
        var3.add(var8);
        (var8 = new JMenuItem("Save")).addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                if (ParticleSystemGUI.this.currentSystem != null) {
                    if (ParticleSystemGUI.this.currentSystem.saveName == null) {
                        ParticleSystemGUI.this.openSaveAs();
                        return;
                    }

                    ParticleSystemGUI.this.currentSystem.save(new FileExt(ParticleSystemGUI.this.currentSystem.saveName));
                }

            }
        });
        var3.add(var8);
        (var8 = new JMenuItem("Save As")).addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                if (ParticleSystemGUI.this.currentSystem != null) {
                    ParticleSystemGUI.this.openSaveAs();
                }

            }
        });
        var3.add(var8);
        (var8 = new JMenuItem("Close")).addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1) {
                try {
                    throw new Exception("System.exit() called");
                } catch (Exception var2) {
                    var2.printStackTrace();
                    System.exit(0);
                }
            }
        });
        var3.add(var8);
        JPanel var10;
        (var10 = new JPanel()).setBorder(new EmptyBorder(5, 5, 5, 5));
        var10.setLayout(new BorderLayout(0, 0));
        this.setContentPane(var10);
        JPanel var9 = new JPanel();
        var10.add(var9, "North");
        GridBagLayout var4;
        (var4 = new GridBagLayout()).columnWidths = new int[]{0, 0, 0, 0, 0, 0};
        var4.rowHeights = new int[]{0, 0};
        var4.columnWeights = new double[]{0.0D, 0.0D, 0.0D, 0.0D, 0.0D, 4.9E-324D};
        var4.rowWeights = new double[]{0.0D, 4.9E-324D};
        var9.setLayout(var4);
        JButton var11 = new JButton("Start");
        GridBagConstraints var5;
        (var5 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
        var5.gridx = 0;
        var5.gridy = 0;
        var11.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1x) {
                    System.err.println("STARTING PARTICLE SYSTEM AT: " + ((ClientState)var1).getCurrentPosition().origin.toString());
                    ((ClientState)var1).getParticleSystemManager().startParticleSystemWorld(ParticleSystemGUI.this.currentSystem, ((ClientState)var1).getCurrentPosition());

            }
        });
        var9.add(var11, var5);
        (var11 = new JButton("Pause")).addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1x) {
                ((ClientState)var1).getParticleSystemManager().pauseParticleSystemsWorld();
            }
        });
        (var5 = new GridBagConstraints()).insets = new Insets(0, 0, 0, 5);
        var5.gridx = 2;
        var5.gridy = 0;
        var9.add(var11, var5);
        (var11 = new JButton("Stop")).addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent var1x) {
                ((ClientState)var1).getParticleSystemManager().stopParticleSystemsWorld();
            }
        });
        GridBagConstraints var6;
        (var6 = new GridBagConstraints()).gridx = 4;
        var6.gridy = 0;
        var9.add(var11, var6);
        this.panel_1 = new JPanel();
        var10.add(this.panel_1, "Center");
        GridBagLayout var7;
        (var7 = new GridBagLayout()).columnWidths = new int[]{1};
        var7.rowHeights = new int[]{1};
        var7.columnWeights = new double[]{4.9E-324D};
        var7.rowWeights = new double[]{4.9E-324D};
        this.panel_1.setLayout(var7);
    }

    //INSERTED CODE @183
    public ParticleSystemGUI(final StateInterface state, final Transform transform) {

        setTitle("StarMade ParticleSystem");
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setBounds(GLFrame.getWidth() - 40, 50, 690, 578);

        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);

        JMenu mnFile = new JMenu("File");
        menuBar.add(mnFile);

        JMenuItem mntmNew = new JMenuItem("New");
        mntmNew.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentSystem = ParticleSystemConfiguration.fromScratch();

                updateCurrentSystem();
            }

        });
        mnFile.add(mntmNew);

        JMenuItem mntmOpen = new JMenuItem("Open");
        mntmOpen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                openOpenFile();
            }
        });
        mnFile.add(mntmOpen);

        JMenuItem mntmSave = new JMenuItem("Save");
        mntmSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (currentSystem != null) {
                    if (currentSystem.saveName == null) {
                        openSaveAs();
                    } else {
                        currentSystem.save(new FileExt(currentSystem.saveName));
                    }
                }
            }

        });
        mnFile.add(mntmSave);

        JMenuItem mntmSaveAs = new JMenuItem("Save As");
        mntmSaveAs.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (currentSystem != null) {
                    openSaveAs();
                }
            }
        });
        mnFile.add(mntmSaveAs);

        JMenuItem mntmClose = new JMenuItem("Close");
        mntmClose.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{throw new Exception("System.exit() called");}catch(Exception ex){ex.printStackTrace();}System.exit(0);
            }
        });
        mnFile.add(mntmClose);

        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));

        setContentPane(contentPane);

        JPanel panel = new JPanel();
        contentPane.add(panel, BorderLayout.NORTH);
        GridBagLayout gbl_panel = new GridBagLayout();
        gbl_panel.columnWidths = new int[]{0, 0, 0, 0, 0, 0};
        gbl_panel.rowHeights = new int[]{0, 0};
        gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
        gbl_panel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
        panel.setLayout(gbl_panel);

        JButton btnStart = new JButton("Start");
        GridBagConstraints gbc_btnStart = new GridBagConstraints();
        gbc_btnStart.insets = new Insets(0, 0, 0, 5);
        gbc_btnStart.gridx = 0;
        gbc_btnStart.gridy = 0;
        btnStart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.err.println("STARTING PARTICLE SYSTEM AT: " + transform.origin.toString());
                ClientState cs = ((ClientState) state);
                cs.getParticleSystemManager().startParticleSystemWorld(currentSystem, transform);
            }
        });
        panel.add(btnStart, gbc_btnStart);

        JButton btnPause = new JButton("Pause");
        btnPause.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ClientState cs = ((ClientState) state);
                cs.getParticleSystemManager().pauseParticleSystemsWorld();
            }
        });
        GridBagConstraints gbc_btnPause = new GridBagConstraints();
        gbc_btnPause.insets = new Insets(0, 0, 0, 5);
        gbc_btnPause.gridx = 2;
        gbc_btnPause.gridy = 0;
        panel.add(btnPause, gbc_btnPause);

        JButton btnStop = new JButton("Stop");
        btnStop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ClientState cs = ((ClientState) state);
                cs.getParticleSystemManager().stopParticleSystemsWorld();
            }
        });
        GridBagConstraints gbc_btnStop = new GridBagConstraints();
        gbc_btnStop.gridx = 4;
        gbc_btnStop.gridy = 0;
        panel.add(btnStop, gbc_btnStop);

        panel_1 = new JPanel();
        contentPane.add(panel_1, BorderLayout.CENTER);
        GridBagLayout gbl_panel_1 = new GridBagLayout();
        gbl_panel_1.columnWidths = new int[]{1};
        gbl_panel_1.rowHeights = new int[]{1};
        gbl_panel_1.columnWeights = new double[]{Double.MIN_VALUE};
        gbl_panel_1.rowWeights = new double[]{Double.MIN_VALUE};
        panel_1.setLayout(gbl_panel_1);
    }
    //

    public static void main(final String[] var0) {
        EventQueue.invokeLater(new Runnable() {
            public final void run() {
                try {
                    ParticleSystemGUI frame = new ParticleSystemGUI(null);
                    frame.setLocationRelativeTo(null);
                    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
                    frame.setVisible(true);
                } catch (Exception var2) {
                    var2.printStackTrace();
                }
            }
        });
    }

    private void openSaveAs() {
        File var1;
        if ((var1 = this.chooseFile(this, "Save As...")) != null) {
            this.currentSystem.saveName = var1.getAbsolutePath();
            this.currentSystem.save(var1);
        }

    }

    private void openOpenFile() {
        File var1;
        if ((var1 = this.chooseFile(this, "Open ParticleSystemConfig...")) != null) {
            try {
                this.currentSystem = ParticleSystemConfiguration.fromFile(var1, true);
                this.updateCurrentSystem();
                return;
            } catch (Exception var2) {
                var2.printStackTrace();
                GLFrame.processErrorDialogExceptionWithoutReportWithContinue(var2, (StateInterface)null);
            }
        }

    }

    private File chooseFile(JFrame var1, String var2) {
        FileExt var3 = new FileExt("./data/effects/particles/");
        System.err.println("PATH: " + var3.getAbsolutePath());
        if (var3.getAbsolutePath().contains("\\schine\\")) {
            var3 = new FileExt("../schine-starmade/data/effects/particles/");
        }

        var3.mkdirs();
        if (this.fc == null) {
            this.fc = new JFileChooser(var3);
            this.fc.addChoosableFileFilter(new FileFilter() {
                public boolean accept(File var1) {
                    return var1.isDirectory() || var1.getName().endsWith(".xml");
                }

                public String getDescription() {
                    return "StarMade BlockConfig (.xml)";
                }
            });
            this.fc.setAcceptAllFileFilterUsed(false);
        }

        return this.fc.showDialog(var1, var2) == 0 ? this.fc.getSelectedFile() : null;
    }

    private void updateCurrentSystem() {
        this.panel_1.removeAll();
        JPanel var1 = this.currentSystem.getPanel();
        GridBagConstraints var2;
        (var2 = new GridBagConstraints()).weighty = 1.0D;
        var2.weightx = 1.0D;
        var2.fill = 1;
        var2.gridx = 0;
        var2.gridy = 0;
        this.panel_1.add(var1, var2);
        this.panel_1.revalidate();
        this.panel_1.repaint();
    }
}
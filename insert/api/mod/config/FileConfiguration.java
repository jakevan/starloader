package api.mod.config;

import api.DebugFile;
import api.mod.StarMod;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.regex.Pattern;

public class FileConfiguration {
    private StarMod mod;
    private String name;
    private HashMap<String, String> values = new HashMap<String, String>();

    private String configPath;
    public FileConfiguration(StarMod mod, String name){
        this.mod = mod;
        this.name = name;
        this.configPath = "moddata" + File.separator + mod.getSkeleton().getName() + File.separator + name +".yml";
        reloadConfig();
    }
    public FileConfiguration(StarMod mod, String name, ArrayList<String> keys, ArrayList<String> values){
        this.mod = mod;
        this.name = name;
        this.configPath = "moddata" + File.separator + mod.getSkeleton().getName() + File.separator + name +".yml";
        for (int i = 0; i < keys.size(); i++) {
            this.values.put(keys.get(i), values.get(i));
        }
    }

    public String getName() {
        return name;
    }

    public void reloadConfig(){
        values.clear();
        read();
    }
    private void read(){
        try {
            Scanner scanner = new Scanner(new File(configPath));
            while (scanner.hasNext()){
                String next = scanner.nextLine();
                //no wack lines
                if(next.length() > 0){
                    String[] split = next.split(Pattern.quote(": "));
                    if(split.length > 2){
                        DebugFile.err("Too many colons on: " + next);
                    }
                    values.put(split[0], split[1]);
                }
            }
        } catch (FileNotFoundException e) {
            DebugFile.warn("Config file: " + configPath + " not found, writing...");
            saveConfig();
            //e.printStackTrace();
        }
    }
    public String getConfigurableValue(String path, String defaultVal){
        String string = getString(path);
        if(string == null){
            set(path, defaultVal);
            return defaultVal;
        }
        return string;
    }
    public int getConfigurableInt(String path, int defaultVal){
        String str = getConfigurableValue(path, String.valueOf(defaultVal));
        return Integer.parseInt(str);
    }
    public long getConfigurableLong(String path, int defaultVal){
        String str = getConfigurableValue(path, String.valueOf(defaultVal));
        return Long.parseLong(str);
    }
    public float getConfigurableFloat(String path, float defaultVal){
        String str = getConfigurableValue(path, String.valueOf(defaultVal));
        return Float.parseFloat(str);
    }
    public boolean getConfigurableBoolean(String path, boolean defaultVal){
        String str = getConfigurableValue(path, String.valueOf(defaultVal));
        return Boolean.parseBoolean(str);
    }

    public boolean getBoolean(String path) {
        return Boolean.parseBoolean(values.get(path));
    }

    public int getInt(String path){
        return Integer.parseInt(values.get(path));
    }
    public double getDouble(String path){
        return Double.parseDouble(values.get(path));
    }
    public String getString(String path){
        return values.get(path);
    }
    public void set(String path, Object value){
        if(value == null){
            values.remove(path);
            return;
        }
        values.put(path, value.toString());
    }
    //-- todo remove and migrate to TOML
    public void setList(String path, ArrayList<String> list){
        StringBuilder sb = new StringBuilder();
        for (String s : list) {
            sb.append(s).append(",");
        }
        sb.deleteCharAt(sb.length()-1);
        set(path, sb);
    }
    public ArrayList<String> getList(String path){
        ArrayList<String> r = new ArrayList<String>();
        String string = getString(path);
        if(string == null) return r;
        r.addAll(Arrays.asList(string.split(",")));
        return r;
    }
    //--
    public void saveDefault(String... def){
        if(values.isEmpty()) {
            for (String s : def) {
                String[] split = s.split(Pattern.quote(": "));
                values.put(split[0], split[1]);
            }
            saveConfig();
        }
    }
    public void saveConfig(){
        try {
            File file = new File(configPath);
            file.getParentFile().mkdirs();
            if(!file.exists()) {
                file.createNewFile();
            }
            FileWriter writer = new FileWriter(file);
            for (String key : values.keySet()) {
                String value = values.get(key);
                writer.write(key + ": " + value + "\n");
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Set<String> getKeys() {
        return values.keySet();
    }

    public long getLong(String key) {
        return Long.parseLong(getString(key));
    }

    public StarMod getMod() {
        return mod;
    }
}
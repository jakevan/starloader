package api.mod.gui;

import api.ModPlayground;
import api.SMModLoader;
import api.mod.ModIdentifier;
import api.mod.ModSkeleton;
import api.mod.SinglePlayerModData;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import org.schema.game.client.view.mainmenu.DialogInput;
import org.schema.game.client.view.mainmenu.MainMenuGUI;
import org.schema.schine.graphicsengine.core.GLFrame;
import org.schema.schine.graphicsengine.core.GlUtil;
import org.schema.schine.graphicsengine.core.MouseEvent;
import org.schema.schine.graphicsengine.core.Timer;
import org.schema.schine.graphicsengine.forms.gui.GUIActivationCallback;
import org.schema.schine.graphicsengine.forms.gui.GUICallback;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.newgui.*;
import org.schema.schine.input.InputState;

import java.util.List;

public class ModBrowserPanel extends GUIElement implements GUIActiveInterface {

    private SMDModEntryScrollableTableList modList;
    private InstalledModEntryScrollableTableList installedModList;
    private boolean init;
    public GUIMainWindow mainPanel;
    private GUIContentPane modsTab;
    private GUIContentPane browseTab;
    private DialogInput dialogInput;
    private List<GUIElement> toCleanUp = new ObjectArrayList<>();

    public ModBrowserPanel(InputState state, DialogInput dialogInput) {
        super(state);
        this.dialogInput = dialogInput;
    }

    @Override
    public float getWidth() {
        return GLFrame.getWidth() - 470;
    }

    @Override
    public float getHeight() {
        return GLFrame.getHeight() - 70;
    }

    @Override
    public void cleanUp() {
        for(GUIElement e : toCleanUp) {
            e.cleanUp();
        }
        toCleanUp.clear();
    }

    @Override
    public void draw() {
        if(!init) {
            onInit();
        }
        GlUtil.glPushMatrix();
        transform();

        mainPanel.draw();

        GlUtil.glPopMatrix();

    }

    @Override
    public void onInit() {
        if(init) {
            return;
        }
        mainPanel = new GUIMainWindow(getState(), GLFrame.getWidth() - 410, GLFrame.getHeight() - 20, 400, 10, "Mod Manager");
        mainPanel.onInit();
        mainPanel.setPos(435, 35, 0);
        mainPanel.setWidth(GLFrame.getWidth() - 470);
        mainPanel.setHeight(GLFrame.getHeight() - 70);
        mainPanel.clearTabs();

        (modsTab = createInstalledTab()).onInit();
        (browseTab = createBrowseTab()).onInit();

        mainPanel.setSelectedTab(0);
        mainPanel.activeInterface = this;

        mainPanel.setCloseCallback(new GUICallback() {

            @Override
            public boolean isOccluded() {
                return !isActive();
            }

            @Override
            public void callback(GUIElement callingGuiElement, MouseEvent event) {
                if(event.pressedLeftMouse()){
                    dialogInput.deactivate();
                }
            }
        });

        toCleanUp.add(mainPanel);
        init = true;
    }

    @Override
    public void update(Timer timer) {
        super.update(timer);
        modList.update(timer);
        modsTab.update(timer);
    }

    @Override
    public boolean isInside() {
        return mainPanel.isInside();
    }

    private GUIContentPane createBrowseTab() {
        GUIContentPane contentPane = mainPanel.addTab("BROWSE");
        contentPane.setTextBoxHeightLast(700);
        modList = new SMDModEntryScrollableTableList(contentPane.getState(), contentPane.getContent(0));
        modList.onInit();
        contentPane.getContent(0).attach(modList);
        toCleanUp.add(modList);

        /* Moved Download Button to be part of expanded list
        contentPane.addNewTextBox(30);
        GUIAncor buttonPane = new GUIAncor(this.getState(), 500, 30);

        GUITextButton download = new GUITextButton(getState(), 250, 24, GUITextButton.ColorPalette.OK, "DOWNLOAD AND INSTALL", new GUICallback() {
            @Override
            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                if(mouseEvent.pressedLeftMouse()) {
                    DebugFile.log("1");
                    if(modList.getSelectedRow() != null) {
                        DebugFile.log("2");
                        SMDModInfo f = modList.getSelectedRow().f;
                        DebugFile.log("3");
                        if (f != null) {
                            DebugFile.log("4");
                            getState().getController().queueUIAudio("0022_menu_ui - enter");
                            try {
                                ModUpdater.downloadAndLoadMod(f.getName());
                                new SimplePopup( getState(), "Successfully downloaded mod: " + f.getName());
                            } catch (IOException | InstantiationException | InvocationTargetException | IllegalAccessException | ClassNotFoundException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            @Override
            public boolean isOccluded() {
                return !isActive();
            }
        });
        buttonPane.addButton(download);
         */
        toCleanUp.add(contentPane);
        return contentPane;
    }
    GUIContentPane contentPane;
    private GUIContentPane createInstalledTab() {
        contentPane = mainPanel.addTab("INSTALLED");
        contentPane.setTextBoxHeightLast(600);
        installedModList = new InstalledModEntryScrollableTableList(contentPane.getState(), contentPane.getContent(0));
        installedModList.onInit();
        contentPane.getContent(0).attach(installedModList);
        toCleanUp.add(installedModList);

        contentPane.addNewTextBox(30);
//        SimpleGUIHorizontalButtonPane buttonPane = new SimpleGUIHorizontalButtonPane(getState(), contentPane.getWidth() - 78, 30, 2);
        GUIHorizontalButtonTablePane buttonPane = new GUIHorizontalButtonTablePane(getState(), 3, 1, contentPane.getContent(1));

        buttonPane.onInit();
        buttonPane.addButton(0, 0, "ENABLE", GUIHorizontalArea.HButtonColor.GREEN, new GUICallback() {
            @Override
            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                if(mouseEvent.pressedLeftMouse()) {
                    if(installedModList.getSelectedRow() != null) {
                        ModSkeleton f = installedModList.getSelectedRow().f;
                        if (f != null) {
                            getState().getController().queueUIAudio("0022_menu_ui - enter");
                            SinglePlayerModData.getInstance().setClientEnabled(ModIdentifier.fromMod(f), true);
                            installedModList.updateMods();
                            ModBrowserPanel.this.init = false;
                        }
                    }
                }
            }

            @Override
            public boolean isOccluded() {
                return !isActive();
            }
        }, new GUIActivationCallback() {
            @Override
            public boolean isVisible(InputState inputState) {
                return true;
            }

            @Override
            public boolean isActive(InputState inputState) {
                return true;
            }
        });
        buttonPane.addButton(1, 0, "DISABLE", GUIHorizontalArea.HButtonColor.BLUE, new GUICallback() {
            @Override
            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                if(mouseEvent.pressedLeftMouse()) {
                    if(installedModList.getSelectedRow() != null) {
                        ModSkeleton f = installedModList.getSelectedRow().f;
                        if (f != null) {
                            getState().getController().queueUIAudio("0022_menu_ui - enter");
                            SinglePlayerModData.getInstance().setClientEnabled(ModIdentifier.fromMod(f), false);
                            installedModList.updateMods();
                            ModBrowserPanel.this.init = false;
                        }
                    }
                }
            }

            @Override
            public boolean isOccluded() {
                return !isActive();
            }
        }, new GUIActivationCallback() {
            @Override
            public boolean isVisible(InputState inputState) {
                return true;
            }

            @Override
            public boolean isActive(InputState inputState) {
                return true;
            }
        });
        buttonPane.addButton(2, 0, "DELETE", GUIHorizontalArea.HButtonColor.ORANGE, new GUICallback() {
            @Override
            public void callback(GUIElement guiElement, MouseEvent mouseEvent) {
                if(mouseEvent.pressedLeftMouse()) {
                    if(installedModList.getSelectedRow() != null) {
                        ModSkeleton f = installedModList.getSelectedRow().f;
                        if (f != null) {
                            if(f.getRealMod() instanceof ModPlayground){
                                //Dont delete starloader
                                return;
                            }
                            getState().getController().queueUIAudio("0022_menu_ui - enter");
                            SMModLoader.deleteMod(f);
                            installedModList.updateMods();
                            ModBrowserPanel.this.init = false;
                        }
                    }
                }
            }

            @Override
            public boolean isOccluded() {
                return !isActive();
            }
        }, new GUIActivationCallback() {
            @Override
            public boolean isVisible(InputState inputState) {
                return true;
            }

            @Override
            public boolean isActive(InputState inputState) {
                return true;
            }
        });
//        buttonPane.addButton();
//        buttonPane.addButton(enable);
//        buttonPane.addButton(disable);
//        buttonPane.addButton(delete);

        contentPane.getContent(1).attach(buttonPane);
        toCleanUp.add(buttonPane);
        toCleanUp.add(contentPane);
        return contentPane;
    }

    @Override
    public boolean isActive() {
        List<DialogInterface> playerInputs = getState().getController().getInputController().getPlayerInputs();
        return !MainMenuGUI.runningSwingDialog && (playerInputs.isEmpty() || playerInputs.get(playerInputs.size()-1).getInputPanel() == this);
    }
}

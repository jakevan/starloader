package api.mod.gui;

import api.mod.ModIdentifier;
import api.mod.ModSkeleton;
import api.mod.SinglePlayerModData;
import api.mod.StarLoader;
import org.hsqldb.lib.StringComparator;
import org.schema.common.util.CompareTools;
import org.schema.schine.graphicsengine.forms.gui.GUIElement;
import org.schema.schine.graphicsengine.forms.gui.GUIElementList;
import org.schema.schine.graphicsengine.forms.gui.newgui.*;
import org.schema.schine.input.InputState;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Collection;
import java.util.Comparator;
import java.util.Set;

public class InstalledModEntryScrollableTableList extends ScrollableTableList<ModSkeleton> implements GUIActiveInterface {

    private Collection<ModSkeleton> mods;
    public boolean updated;
    private static InstalledModEntryScrollableTableList inst;

    public InstalledModEntryScrollableTableList(InputState state, GUIElement element) {
        super(state, 750, 500, element);
        inst = this;
        updateMods();
        setColumnHeight(60);
    }

    public static InstalledModEntryScrollableTableList getInst() {
        if (inst != null) return inst;
        return null;
    }

    @Override
    public void initColumns() {
        new StringComparator();
        this.addColumn("Icon", 0.25F, new Comparator<ModSkeleton>() {
            public int compare(ModSkeleton o1, ModSkeleton o2) {
                return o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase());
            }
        });

        this.addColumn("Name", 4.0F, new Comparator<ModSkeleton>() {
            public int compare(ModSkeleton o1, ModSkeleton o2) {
                return o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase());
            }
        });

        this.addColumn("Description", 8F, new Comparator<ModSkeleton>() {
            public int compare(ModSkeleton o1, ModSkeleton o2) {
                return o1.getName().toLowerCase().compareTo(o2.getName().toLowerCase());
            }
        });

        this.addColumn("Enabled", 2F, new Comparator<ModSkeleton>() {
            public int compare(ModSkeleton o1, ModSkeleton o2) {
                return CompareTools.compare(SinglePlayerModData.getInstance().isClientEnabled(ModIdentifier.fromMod(o1)), SinglePlayerModData.getInstance().isClientEnabled(ModIdentifier.fromMod(o2)));
            }
        });
        this.addColumn("Mod Type", 1F, new Comparator<ModSkeleton>() {
            public int compare(ModSkeleton o1, ModSkeleton o2) {
                return Boolean.compare(o1.isCoreMod(), o2.isCoreMod());
            }
        });

        this.addTextFilter(new GUIListFilterText<ModSkeleton>() {
            public boolean isOk(String s, ModSkeleton blueprint) {
                return blueprint.getName().toLowerCase().contains(s.toLowerCase());
            }
        }, ControllerElement.FilterRowStyle.FULL);

        this.activeSortColumnIndex = 0;
    }

    @Override
    protected Collection<ModSkeleton> getElementList() {
        if (!updated) updateMods();
        inst = this;
        return mods;
    }

    public void updateMods() {
        mods = StarLoader.starMods;
        flagDirty();
        updated = true;
    }
    private static final BufferedImage defaultImage = new BufferedImage(1,1, BufferedImage.TYPE_INT_ARGB);
    @Override
    public void updateListEntries(GUIElementList list, Set<ModSkeleton> set) {
        if (!updated) updateMods();
        for (final ModSkeleton mod : set) {
            BufferedImage iconImage = mod.getIconImage();
            if(iconImage == null) iconImage = defaultImage;
            GUIIcon iconElement = new GUIIcon(this.getState(), iconImage);
                iconElement.setTextSimple("");
                GUIClippedRow iconRowElement;
                (iconRowElement = new GUIClippedRow(this.getState())).attach(iconElement);


            GUITextOverlayTable nameTextElement = new GUITextOverlayTable(10, 10, this.getState());
            String nameText = "  " + mod.getName() + " v" + mod.getModVersion();
            if(mod.isOutOfDate()){
                nameText += "\n     [Out of Date]";
                nameTextElement.setAWTColor(Color.red);
            }
            nameTextElement.setTextSimple(nameText);
            nameTextElement.setLimitTextDraw(30);
            GUIClippedRow nameRowElement;
            (nameRowElement = new GUIClippedRow(this.getState())).attach(nameTextElement);

            GUITextOverlayTable descriptionText = new GUITextOverlayTable(10, 10, this.getState());
            descriptionText.setTextSimple(mod.getModDescription());
            GUIClippedRow descriptionRowElement = new GUIClippedRow(this.getState());
            descriptionRowElement.attach(descriptionText);

            GUITextOverlayTable enabledText = new GUITextOverlayTable(10, 10, this.getState());
            enabledText.setTextSimple(SinglePlayerModData.getInstance().isClientEnabled(ModIdentifier.fromMod(mod)));
            GUIClippedRow ratingRowElement = new GUIClippedRow(this.getState());
            ratingRowElement.attach(enabledText);

            GUITextOverlayTable coreModText = new GUITextOverlayTable(10, 10, this.getState());
            StringBuilder text = new StringBuilder();
            if(mod.isCoreMod()){
                text.append("[Core] ");
            }
            if(mod.getSmdResourceId() == -1){
                //Mods that are not .jar's. StarLoader and possibly one day a "Vanilla" mod.
                text.append("[Virtual]");
            } else if(mod.isClientMod()){
                text.append("[Client]");
            } else if(mod.isServerMod()){
                text.append("[Server]");
            } else{
                text.append("[Normal]");
            }
            coreModText.setTextSimple(text.toString());
            GUIClippedRow coreModRowElement = new GUIClippedRow(this.getState());
            coreModRowElement.attach(coreModText);

            ModEntryListRow blueprintListRow = new ModEntryListRow(this.getState(),  mod, iconRowElement, nameRowElement, descriptionRowElement, enabledText, coreModRowElement);
            blueprintListRow.onInit();

            list.addWithoutUpdate(blueprintListRow);
        }

        list.updateDim();
    }

    public class ModEntryListRow extends ScrollableTableList<ModSkeleton>.Row {
        public ModEntryListRow(InputState inputState, ModSkeleton blueprint, GUIElement... guiElements) {
            super(inputState, blueprint, guiElements);
            this.highlightSelect = true;
            this.highlightSelectSimple = true;
            this.setAllwaysOneSelected(true);
        }
    }
}


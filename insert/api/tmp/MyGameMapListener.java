package api.tmp;

import api.listener.fastevents.GameMapDrawListener;
import org.schema.common.util.linAlg.Vector3i;
import org.schema.game.client.view.gamemap.GameMapDrawer;
import org.schema.schine.graphicsengine.core.Controller;
import org.schema.schine.graphicsengine.forms.PositionableSubColorSprite;
import org.schema.schine.graphicsengine.forms.Sprite;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

/**
 * Created by Jake on 11/12/2020.
 * <insert description here>
 */
public class MyGameMapListener implements GameMapDrawListener {

    @Override
    public void system_PreDraw(GameMapDrawer drawer, Vector3i system, boolean explored) {

    }

    @Override
    public void system_PostDraw(GameMapDrawer drawer, Vector3i system, boolean explored) {

    }

    @Override
    public void galaxy_PreDraw(GameMapDrawer drawer) {

    }

    @Override
    public void galaxy_PostDraw(GameMapDrawer drawer) {

    }

    @Override
    public void galaxy_DrawLines(GameMapDrawer drawer) {
//        Vector3f start = new Vector3f(0, 0, 0);
//        Vector3f end = new Vector3f(100, 1000, 100);
//        Vector4f startColor = new Vector4f(1, 1, 1, 1);
//        Vector4f endColor = new Vector4f(1, 1, 1, 0);
//        DrawUtils.drawFTLLine(start, end, startColor, endColor);
    }

    @Override
    public void galaxy_DrawSprites(GameMapDrawer drawer) {
        Sprite sprite = Controller.getResLoader().getSprite("map-sprites-8x2-c-gui-");
        PositionableSubColorSprite[] sprites = new PositionableSubColorSprite[100];
        for (int i = 0; i < sprites.length; i++) {
            final int finalI = i;
            sprites[i] = new PositionableSubColorSprite() {
                @Override
                public Vector4f getColor() {
                    return new Vector4f(1,1,1,1);
                }

                @Override
                public float getScale(long l) {
                    return 1;
                }

                @Override
                public int getSubSprite(Sprite sprite) {
                    return 0;
                }

                @Override
                public boolean canDraw() {
                    return true;
                }

                @Override
                public Vector3f getPos() {
                    return new Vector3f(0, (float) (Math.sin(finalI)*GameMapDrawer.sectorSize), finalI*GameMapDrawer.sectorSize);
                }
            };
        }
        DrawUtils.drawSprite(drawer, sprite, sprites);
    }

    @Override
    public void galaxy_DrawQuads(GameMapDrawer drawer) {
        DrawUtils.drawCube(new Vector3f(100,100,100), 100, new Vector4f(0F,1F,1F, 0.5F));
        DrawUtils.tintSystem(new Vector3i(2,3,4), new Vector4f(1,0,0, 0.8F));
    }
}

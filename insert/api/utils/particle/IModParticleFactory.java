package api.utils.particle;

import javax.vecmath.Vector3f;

/**
 * Created by Jake on 12/4/2020.
 * <insert description here>
 */
public interface IModParticleFactory{
    ModParticle newParticle(int factoryId, int sprite,
                            Vector3f worldPos, ModParticleUtil.Builder builder);
}

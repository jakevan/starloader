package api.utils.game.inventory;

import api.DebugFile;
import org.schema.game.common.data.element.ElementKeyMap;
import org.schema.game.common.data.player.inventory.Inventory;

/**
 * Created by Jake on 9/30/2020.
 * <insert description here>
 */
public class InventoryUtils {
    public static void addItem(Inventory inventory, short id, int amount) {
        int slot = inventory.incExistingOrNextFreeSlot(id, amount);
        if(inventory.getInventoryHolder() != null) {
            inventory.sendInventoryModification(slot);
        }
    }
    public static boolean addElementById(Inventory inventory, short id, int amount) {
        if (ElementKeyMap.isValidType(id)) {
            int i = inventory.incExistingOrNextFreeSlot(id, amount);
            inventory.sendInventoryModification(i);
            return true;
        } else {
            return false;
        }
    }

    public static int getItemAmount(Inventory inventory, short id) {
        if(!inventory.containsAny(id)) {
            return 0;
        } else {
            int count = 0;
            for(int slotNum : inventory.getSlots()) {
                count += inventory.getCount(slotNum, id);
            }
            return count;
        }
    }

    public static void consumeItems(Inventory inventory, short id, int toConsume) {
        //Don't know if this actually works, hopefully it does...
        if(!inventory.containsAny(id)) {
            DebugFile.err("Tried to consume x" + toConsume + " " + id + "in an inventory, however the inventory did not contain any!");
        } else {
            while(toConsume > 0) {
                for(int slotNum : inventory.getSlots()) {
                    if(inventory.getSlot(slotNum).getType() == id) {
                        int toSubtract = inventory.getSlot(slotNum).count() - toConsume;
                        if(toSubtract > 0) {
                            inventory.getSlot(slotNum).setCount(inventory.getSlot(slotNum).count() - toSubtract);
                            toConsume = toConsume - toSubtract;
                        } else {
                            inventory.getSlot(slotNum).clear();
                            int overFlow = Math.abs(toSubtract);
                            toConsume = toConsume - overFlow;
                        }
                    }
                }
            }
        }
    }
}

package api.utils.textures;

import java.awt.*;
import java.awt.image.BufferedImage;

public interface GraphicsOperator {
    void apply(BufferedImage image, Graphics g);
}

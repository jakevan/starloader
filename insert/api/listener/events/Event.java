package api.listener.events;

import api.DebugFile;
import api.listener.Listener;
import api.mod.StarLoader;

import java.util.List;

public abstract class Event {
    private boolean canceled = false;
    private boolean server = true;
    protected Condition condition = Condition.NONE;

    public boolean isCanceled() {
        return canceled;
    }

    public void setCanceled(boolean canceled) {
        this.canceled = canceled;
    }

    public boolean isServer() {
        return server;
    }

    public enum Condition {
        PRE,
        NONE,//For events that dont have pre/post conditions
        POST,
    }

    public Condition getCondition() {
        return condition;
    }

    public static void fireEvent(Event ev, boolean isServer) {
        ev.server = isServer;

        List<Listener> listeners = StarLoader.getListeners(ev.getClass());
        if (listeners == null) // Avoid iterating on null Event listeners
            return;
        for (Listener listener : listeners) {
            try {
                listener.onEvent(ev);
            } catch (Exception e) {
                DebugFile.log("Exception during event: " + ev.getClass().getSimpleName());
                DebugFile.logError(e, null);
            }
        }
    }
}
